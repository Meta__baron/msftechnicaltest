package com.cbretin.msftechnicaltest

import com.cbretin.msftechnicaltest.data.service.TextCheckService
import com.cbretin.msftechnicaltest.data.service.TextCheckServiceImpl
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class TextCheckServiceUnitTest {

    private val STRING_TEST_SUCCESS = "Good string"
    private val STRING_TEST_FAIL_EMPTY = ""
    private val STRING_TEST_FAIL_LOWERCASE = "bad string"
    private val STRING_TEST_FAIL_NUMBER = "2 Bad string"
    private val STRING_TEST_FAIL_SPECIAL_CHARACTER = "% Bad string"
    private val STRING_TEST_FAIL_SPACE = " Bad string"

    lateinit var textCheckService: TextCheckService

    @Before
    fun setUp(){
        textCheckService = TextCheckServiceImpl()
    }

    @Test
    fun isTextStartWithUppercaseLetterTest_Success() {
        assertTrue(textCheckService.isTextStartWithUppercaseLetter(STRING_TEST_SUCCESS))
    }

    @Test
    fun isTextStartWithUppercaseLetterTest_Fail_Empty() {
        assertFalse(textCheckService.isTextStartWithUppercaseLetter(STRING_TEST_FAIL_EMPTY))
    }

    @Test
    fun isTextStartWithUppercaseLetterTest_Fail_Lowercase_Start() {
        assertFalse(textCheckService.isTextStartWithUppercaseLetter(STRING_TEST_FAIL_LOWERCASE))
    }

    @Test
    fun isTextStartWithUppercaseLetterTest_Fail_NumberStart() {
        assertFalse(textCheckService.isTextStartWithUppercaseLetter(STRING_TEST_FAIL_NUMBER))
    }

    @Test
    fun isTextStartWithUppercaseLetterTest_Fail_SpecialCharacterStart() {
        assertFalse(textCheckService.isTextStartWithUppercaseLetter(STRING_TEST_FAIL_SPECIAL_CHARACTER))
    }

    @Test
    fun isTextStartWithUppercaseLetterTest_Fail_SpaceStart() {
        assertFalse(textCheckService.isTextStartWithUppercaseLetter(STRING_TEST_FAIL_SPACE))
    }
}