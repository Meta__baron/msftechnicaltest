package com.cbretin.msftechnicaltest.domain.model

data class FormData(
    var name: String = "",
    var pictureAngle: Int = 0
)
