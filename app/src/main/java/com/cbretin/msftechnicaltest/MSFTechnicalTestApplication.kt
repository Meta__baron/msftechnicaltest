package com.cbretin.msftechnicaltest

import android.app.Application
import com.cbretin.msftechnicaltest.data.cache.FormDataCacheImpl
import com.cbretin.msftechnicaltest.data.repository.FormRepository
import com.cbretin.msftechnicaltest.data.repository.FormRepositoryImpl
import com.cbretin.msftechnicaltest.data.service.TextCheckServiceImpl

class MSFTechnicalTestApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        initInjection()
    }

    private fun initInjection(){
        formRepository = FormRepositoryImpl(TextCheckServiceImpl(), FormDataCacheImpl())
    }

    companion object {
        lateinit var formRepository: FormRepository
    }
}