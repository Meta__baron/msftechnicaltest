package com.cbretin.msftechnicaltest.data.cache

import com.cbretin.msftechnicaltest.domain.model.FormData

class FormDataCacheImpl: FormDataCache {

    private var formData = FormData()

    override fun setFormName(name: String) {
        formData.name = name
    }

    override fun setFormAngle(angle: Int) {
        formData.pictureAngle = angle
    }

    override fun getFormData(): FormData {
        return formData
    }
}