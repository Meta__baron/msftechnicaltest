package com.cbretin.msftechnicaltest.data.cache

import com.cbretin.msftechnicaltest.domain.model.FormData


interface FormDataCache {
    fun setFormName(name: String)
    fun setFormAngle(angle: Int)
    fun getFormData(): FormData
}