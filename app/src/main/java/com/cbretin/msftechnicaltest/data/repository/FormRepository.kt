package com.cbretin.msftechnicaltest.data.repository

import com.cbretin.msftechnicaltest.domain.model.FormData

interface FormRepository {
    suspend fun isTextStartWithUppercaseLetter(text:String): Boolean
    suspend fun setPictureAngle(pictureAngle: Int)
    suspend fun getFormData(): FormData
}