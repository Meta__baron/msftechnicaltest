package com.cbretin.msftechnicaltest.data.service

class TextCheckServiceImpl: TextCheckService {

    override fun isTextStartWithUppercaseLetter(text: String): Boolean {
        return text.isNotEmpty() && text.first().isUpperCase()
    }
}