package com.cbretin.msftechnicaltest.data.repository

import com.cbretin.msftechnicaltest.data.cache.FormDataCache
import com.cbretin.msftechnicaltest.data.service.TextCheckService
import com.cbretin.msftechnicaltest.domain.model.FormData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FormRepositoryImpl (
    private val textCheckService: TextCheckService,
    private val formDataCache: FormDataCache
): FormRepository {

    override suspend fun isTextStartWithUppercaseLetter(text: String): Boolean {
        return withContext(Dispatchers.IO){
            val isTextValid = textCheckService.isTextStartWithUppercaseLetter(text)
            if(isTextValid) formDataCache.setFormName(text)
            isTextValid
        }
    }

    override suspend fun setPictureAngle(pictureAngle: Int) {
        return withContext(Dispatchers.IO){
            formDataCache.setFormAngle(pictureAngle)
        }
    }

    override suspend fun getFormData(): FormData {
        return withContext(Dispatchers.IO){
            formDataCache.getFormData()
        }
    }
}