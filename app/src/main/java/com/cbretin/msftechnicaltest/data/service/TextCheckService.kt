package com.cbretin.msftechnicaltest.data.service

interface TextCheckService {
    fun isTextStartWithUppercaseLetter(text: String): Boolean
}