package com.cbretin.msftechnicaltest.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.cbretin.msftechnicaltest.R
import com.cbretin.msftechnicaltest.presentation.viewmodel.PictureActivityViewModel
import com.cbretin.msftechnicaltest.presentation.viewmodel.factory.CustomViewModelFactory

import kotlinx.android.synthetic.main.activity_picture.activity_picture_imageview as imageView


class PictureActivity : AppCompatActivity() {

    private lateinit var viewModel: PictureActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture)
        viewModel = ViewModelProvider(this, CustomViewModelFactory())[PictureActivityViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initImageview()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initImageview(){
        viewModel.getPictureData {
            title = it.name
            imageView.rotation = it.pictureAngle.toFloat()
        }
    }
}