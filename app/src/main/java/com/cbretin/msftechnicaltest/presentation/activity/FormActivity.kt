package com.cbretin.msftechnicaltest.presentation.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import com.cbretin.msftechnicaltest.R
import com.cbretin.msftechnicaltest.presentation.viewmodel.FormActivityViewModel
import com.cbretin.msftechnicaltest.presentation.viewmodel.factory.CustomViewModelFactory
import kotlinx.android.synthetic.main.activity_form.activity_form_angle_step_framelayout as angleFormStepLayout
import kotlinx.android.synthetic.main.layout_form_angle_step.layout_form_angle_step_ok_button as angleOkButton
import kotlinx.android.synthetic.main.layout_form_angle_step.layout_form_angle_step_seekbar as angleSeekbar
import kotlinx.android.synthetic.main.layout_form_angle_step.layout_form_angle_step_selected_angle_textview as angleTextview
import kotlinx.android.synthetic.main.layout_form_name_step.layout_form_name_step_edittext as nameEditText
import kotlinx.android.synthetic.main.layout_form_name_step.layout_form_name_step_error_textview as nameErrorTextview
import kotlinx.android.synthetic.main.layout_form_name_step.layout_form_name_step_ok_button as nameOkButton


private const val MAX_ANGLE_VALUE = 360

class FormActivity : AppCompatActivity() {

    private lateinit var viewModel: FormActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        viewModel = ViewModelProvider(this, CustomViewModelFactory())[FormActivityViewModel::class.java]

        initNameFormStep()
        initAngleFormStep()
    }

    private fun initNameFormStep(){
        nameEditText.imeOptions = nameEditText.imeOptions or EditorInfo.IME_ACTION_DONE
        nameEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                checkNameIsValid()
            }
            false
        }

        nameEditText.doAfterTextChanged {
            hideNameError()
        }

        nameOkButton.setOnClickListener {
            checkNameIsValid()
        }
    }

    private fun initAngleFormStep(){
        angleSeekbar.max = MAX_ANGLE_VALUE
        angleSeekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateAngleText()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })

        angleOkButton.setOnClickListener {
            viewModel.setPictureAngle(angleSeekbar.progress,
                onSuccess = {
                    launchPictureActivity()
                }
            )
        }

        updateAngleText()
    }

    private fun checkNameIsValid(){
        viewModel.isTextStartWithUppercase(nameEditText.text.toString(),
            onSuccess = {
                showAngleForm()
            },
            onError = {
                showNameError()
            }
        )
    }

    private fun showAngleForm(){
        angleFormStepLayout.visibility = View.VISIBLE
        disableNameForm()
    }

    private fun disableNameForm(){
        nameOkButton.isClickable = false
        nameOkButton.alpha = 0.5f

        nameEditText.isEnabled = false
        nameEditText.alpha = 0.5f
    }

    private fun updateAngleText(){
        angleTextview.text = getString(R.string.form_angle_selection, angleSeekbar.progress)
    }

    private fun showNameError(){
        if(nameErrorTextview.visibility != View.VISIBLE) nameErrorTextview.visibility = View.VISIBLE
    }

    private fun hideNameError(){
        if(nameErrorTextview.visibility != View.INVISIBLE) nameErrorTextview.visibility = View.INVISIBLE
    }

    private fun launchPictureActivity(){
        startActivity(Intent(this, PictureActivity::class.java))
    }
}