package com.cbretin.msftechnicaltest.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cbretin.msftechnicaltest.data.repository.FormRepository
import kotlinx.coroutines.launch

class FormActivityViewModel(
    private val formRepository: FormRepository
) : ViewModel() {

    fun isTextStartWithUppercase(text: String, onSuccess:() -> Unit, onError:() -> Unit){
        viewModelScope.launch {
            when(formRepository.isTextStartWithUppercaseLetter(text)){
                true -> onSuccess()
                else -> onError()
            }
        }
    }

    fun setPictureAngle(selectedAngle: Int, onSuccess:() -> Unit){
        viewModelScope.launch {
            formRepository.setPictureAngle(selectedAngle)
            onSuccess()
        }
    }
}