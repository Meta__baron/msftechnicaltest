package com.cbretin.msftechnicaltest.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cbretin.msftechnicaltest.data.repository.FormRepository
import com.cbretin.msftechnicaltest.domain.model.FormData
import kotlinx.coroutines.launch

class PictureActivityViewModel(
    private val formRepository: FormRepository
) : ViewModel() {

    fun getPictureData(onSuccess:(pictureData: FormData) -> Unit){
        viewModelScope.launch {
            val data = formRepository.getFormData()
            onSuccess(data)
        }
    }
}