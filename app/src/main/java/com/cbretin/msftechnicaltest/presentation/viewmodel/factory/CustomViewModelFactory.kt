package com.cbretin.msftechnicaltest.presentation.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cbretin.msftechnicaltest.MSFTechnicalTestApplication
import com.cbretin.msftechnicaltest.presentation.viewmodel.PictureActivityViewModel
import com.cbretin.msftechnicaltest.presentation.viewmodel.FormActivityViewModel

class CustomViewModelFactory: ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FormActivityViewModel::class.java)) {
            return FormActivityViewModel(MSFTechnicalTestApplication.formRepository) as T
        }
        if (modelClass.isAssignableFrom(PictureActivityViewModel::class.java)) {
            return PictureActivityViewModel(MSFTechnicalTestApplication.formRepository) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}