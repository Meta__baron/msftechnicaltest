package com.cbretin.msftechnicaltest

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.cbretin.msftechnicaltest.presentation.activity.FormActivity
import org.junit.Rule
import org.junit.Test

class FormActivityUITest {

    private val STRING_TEST_SUCCESS = "Good string"
    private val STRING_TEST_FAIL = "bad string"

    @get:Rule
    var mActivityRule: ActivityTestRule<FormActivity> = ActivityTestRule(FormActivity::class.java)

    @Test
    fun ensureAngleFormStepDisplayed() {
        Espresso.onView(withId(R.id.layout_form_name_step_edittext)).perform(ViewActions.typeText(STRING_TEST_SUCCESS))
        Espresso.onView(withId(R.id.layout_form_name_step_ok_button)).perform(click())

        Espresso.onView(withId(R.id.activity_form_angle_step_framelayout)).check(matches(isDisplayed()))
    }

    @Test
    fun ensureNameFormErrorDisplayed() {
        Espresso.onView(withId(R.id.layout_form_name_step_edittext)).perform(ViewActions.typeText(STRING_TEST_FAIL))
        Espresso.onView(withId(R.id.layout_form_name_step_ok_button)).perform(click())

        Espresso.onView(withId(R.id.layout_form_name_step_error_textview)).check(matches(isDisplayed()))
    }
}